const s = "HbideVbxncC"; // "A-Bb-Ccc-Dddd""

function accum(s) {
  const tab = s.split("");
  let temp = []
  tab.forEach((element, index) => {   
    for(let i = 0; i<index+1; i++){
      if(i === 0){
        temp.push(element.toUpperCase())
      }
      else if (i === index){
        temp.push(element.toLowerCase());
        temp.push("-");
      }
      else{
        temp.push(element.toLowerCase());
      }
    }      
  });
  temp.pop()
  temp.splice(1,0, "-");
  return temp.join("");
}

//----------------------------------------------------------

str = "o a kak ushakov lil vo kashu kakao"

function getCount(str) {
  let vowelsCount = 0;
  const tab = str.split("");
  tab.forEach(el => { 
    if(el === "a" || el === "e" || el === "i" || el === "o" || el === "u"){
      vowelsCount++
    }   
  });    
  return vowelsCount;
}

//----------------------------------------------------------
s = "partyy"

function getMiddle(s){ 
  let index;
  if(s.length % 2 === 0){
    index = s.length / 2;
    return s.split("").slice(index-1, index+1).join("")
  }
  else {
    index = (s.length +1) / 2;
    const result = s.split("");
    return result[index-1];
  }
}

//----------------------------------------------------------

const numbers = "4 5 29 54 4 0 -214 542 -64 1 -3 6 -6";

function highAndLow(numbers){
  const tab = numbers.split(" ").map(el => parseInt(el));
  const max = tab.reduce((curr, acc) => Math.max(curr, acc));
  const min = tab.reduce((curr, acc) => Math.min(curr, acc));
  return max + " " + min;
}

//----------------------------------------------------------

function likes(names) {
  if(names.length === 0){
    return "no one likes this"
  } else if (names.length === 1){
    return `${names[0]} likes this`;
  } else if (names.length === 2) {
    return `${names[0]} and ${names[1]} like this`;
  } else if (names.length === 3) {
    return `${names[0]}, ${names[1]} and ${names[2]} like this`;
  } else {
    return `${names[0]}, ${names[2]} and ${names.length - 2} others like this`;
  }
}

//----------------------------------------------------------

const a = [ 1, 1, 2, -2, 5, 2, 4, 4, -1, -2, 5 ];
function findOdd(a) {
  a.sort((a, b) => a - b);
  const newTab = [];
  for( let i = 0; i<a.length; i++) {
    if(a[i] !== a[i+1]){
      newTab.push(a[i]);  
    } else {
      i++;
    }
    }
  return parseInt(newTab.join());
}

//----------------------------------------------------------

function persistence(num) { 
  let tab = num.toString().split(""); 
  let i = num.toString().length; 
  let counter = 0;
  if(i === 1) {
    return counter;
  } else {
      while (i > 1){ 
        counter ++;
        i = tab.reduce((curr, acc) => curr * acc).toString().length 
        tab = tab.reduce((curr, acc) => curr * acc).toString().split(""); 
      }
      return counter;
  } 
}

//----------------------------------------------------------

const arr = [20,37,20,21]   // [20, 20, 21, 37], 1

function deleteNth(arr,n){
  const temp = [];
  arr.forEach(el => {
    const test = containsLessThanN(el, n, temp)
    if(containsLessThanN(el, n, temp)){
      temp.push(el);
    }
  })
  return temp;
}
function containsLessThanN(element, n, array) {
  let counter = 0;
  array.forEach(el => {
    if(el === element){
      counter++;
    }
  })
  return counter < n ? true: false;
}

//----------------------------------------------------------

// "(){}[]"   =>  True
// "([{}])"   =>  True
// "(}"       =>  False
// "[(])"     =>  False
// "[({})](]" =>  False


//stos
const braces = "[({})](]";

function validBraces(braces){
  const tab = braces.split("");
  const open = ["(", "[", "{"];
  const temp = [];
  if(tab.length % 2 === 0){
    for(let i = 0; i<tab.length; i++){
      if(open.includes(tab[i])){
        temp.push(tab[i])
        tab[i] = null;
      } else {
          if(corespondingChar(tab[i]) === temp[temp.length-1]){     
            temp.pop();
            tab[i] = null;
          }     
      }
    }
    if(tab.filter(el => el !== null).length === 0 && temp.length === 0){
      return true;
    } else {
      return false;
    }
  } else {
    return false;
  }
}

function corespondingChar(given){
  return given === ")" ? "("
    : given === "]" ? "["
    : given === "}" ? "{"
    : false; 
}

// ----------------------------------------------------------------

//function test(n, expected) {
//  let actual = solution(n)
//  Test.assertEquals(actual, expected, `Expected ${expected}, got ${actual}`)
// }
//  Test.describe("basic tests", function(){
//  test(10,23)
//})

function solution(number){
  const arr = [];
  for(let i = 1; i<number; i++){  
    if(i % 3 === 0 || i % 5 === 0){
      arr.push(i)
    }
  }
  if(arr.length > 0){
    return arr.reduce((acc, val) => acc + val);
  } else {
    return 0;
  }  
}

// ----------------------------------------------------------------
 
 const integers = [160, 3, 1719, 19, 11, 13, -21]

function findOutlier(integers){
  if(integers.filter(el => el %2 === 0).length > 1){
    return integers.filter(el => el % 2 !== 0)[0];
  } else {
    return integers.filter(el => el %2 === 0)[0];
  }
}

// ----------------------------------------------------------------

const word = "reCede";  // =>  "()()()"

function duplicateEncode(word){
  const arr = word.toLowerCase().split("");
  const result = [];
  arr.map(el => {
    let isRepetetive = containsMoreThanOne(el, arr);
    if(isRepetetive){
      result.push(")");
    } else {
      result.push("(");
    }
  });
  return result.join("");
}

function containsMoreThanOne(el, arr) {
  return arr.filter(element => element === el).length > 1 ? true: false; 
}

//--------------------------------------------------------------------

//large factorials - to be continued...
// const digit = 5;

// function factorial(n) {
//     let fact = 1;
//     for( let i = 1; i <= n; i++){
//       if(Number.isSafeInteger(fact*i)){
//         fact = fact* i;
//       }
//       else {
//         let factxi = "0";  // this is (fact * i) for us.
//         for(let j = 0; j < i; j++){
//             factxi = add(factxi,fact.toString());  
//         }
//         fact = factxi;

//      }
       
//     }

//     return fact.toString();
//     //const res = digits.reduce((acc, val) => acc* val).toString();
//     //return res;
// }

//--------------------------------------------------------------------

// tworzenie tagów - ćwiczenie
/* const createTag = config => {
  const tag = document.createElement(config.tagName);


  if (config.text) {
    const tagText = document.createTextNode(config.text);
    tag.appendChild(tagText);
  }

  if (config.className) {
    tag.classList.add(config.className);
  }

  if (config.cb) {
    tag.addEventListener("click", config.cb);
  }

  if (config.idName) {
    tag.id = config.idName;
  }

  return tag;
};


const tagMaker = {
tag: document.createElement(config.tagName),
text: document.createTextNode(config.text),
className: tag.classList.add(config.classList),
cb: tag.addEventListener("click", config.cb),
idName: config.idName
};
*/
  
// ................................................................

// digital_root(16)
// => 1 + 6
// => 7

// digital_root(942)
// => 9 + 4 + 2
// => 15 ...
// => 1 + 5
// => 6

// digital_root(132189)
// => 1 + 3 + 2 + 1 + 8 + 9
// => 24 ...
// => 2 + 4
// => 6

// digital_root(493193)
// => 4 + 9 + 3 + 1 + 9 + 3
// => 29 ...
// => 2 + 9
// => 11 ...
// => 1 + 1
// => 2


function digital_root(n) {
  let tab2 = [];
  
  const changeToTab = number => {
  let temp = number.toString().split("");
  let tab= [];
   for(let i = 0; i<temp.length; i++){
      tab.push(parseInt(temp[i]));
    } 
    return tab;
  }  
  tab2 = changeToTab(n) 
  while (tab2.length != 1){
    let res = tab2.reduce((acc, val) => acc + val);
    tab2 = changeToTab(res)
   } 
  let result = parseInt(tab2.toString());
  return result;
  }

//-------------------------------------------------------
// let h = 100
// let bounce = 0.999999999;
// let window = 1;


 function bouncingBall(h,  bounce,  window) {
  let count = -1;
    if((h > window) && bounce < 1 && h > 0 && bounce >0){
      let newHeight = h;
      while (newHeight > window){
        newHeight = h * bounce;
        count = count +2;
        h = newHeight;
       }
    } 
  return count;
  }
//-------------------------------------------------------

function spinWords(word){
  let result = word.split(" ").map(function(word){
    if(word.length >= 5){
      return word.split("").reverse().join("");
    } else {
      return word
    }
 })
 return result.join(" ");
}

//-------------------------------------------------------

//some test cases for you...
// Test.expect(isValidWalk(['n','s','n','s','n','s','n','s','n','s']), 'should return true');
// Test.expect(!isValidWalk(['w','e','w','e','w','e','w','e','w','e','w','e']), 'should return false');
// Test.expect(!isValidWalk(['w']), 'should return false');
// Test.expect(!isValidWalk(['n','n','n','s','n','s','n','s','n','s']), 'should return false');

function isValidWalk(walk) {
  if((walk.length % 2 === 0) && walk.length === 10){
    if((walk.filter(el => el === "n").length === walk.filter(el=> el==="s").length) && (walk.filter(el=>el==="w").length === walk.filter(el=>el==="e").length)){
      return true
    } else {
      return false;
    }
  } else {
    return false;
  }
}

//-------------------------------------------------------

// Well met with Fibonacci bigger brother, AKA Tribonacci.

// As the name may already reveal, it works basically like a Fibonacci, but summing the last 3 (instead of 2) 
//numbers of the sequence to generate the next. And, worse part of it, regrettably I won't get to hear non-native
// Italian speakers trying to pronounce it :(
// So, if we are to start our Tribonacci sequence with [1, 1, 1] as a starting input (AKA signature), we have this sequence:
// [1, 1 ,1, 3, 5, 9, 17, 31, ...]
// But what if we started with [0, 0, 1] as a signature? As starting with [0, 1] instead of [1, 1] 
//basically shifts the common Fibonacci sequence by once place, you may be tempted to think that we would get the same sequence 
//shifted by 2 places, but that is not the case and we would get:
// [0, 0, 1, 1, 2, 4, 7, 13, 24, ...]
// Well, you may have guessed it by now, but to be clear: you need to create a fibonacci function that given a signature 
//array/list, returns the first n elements - signature included of the so seeded sequence.
// Signature will always contain 3 numbers; n will always be a non-negative number; if n == 0, then return an empty array 
//(except in C return NULL) and be ready for anything else which is not clearly specified ;)

function tribonacci(signature,n){
  let tab = [];
  for(let i = 0; i < n; i++){
    if(i > 2){
      signature[i] = signature[i-1] + signature[i-2] + signature[i-3];
      tab.push(signature[i])
    } else {
      tab.push(signature[i])
    }
  }
return tab;
}

//-------------------------------------------------------

// "is2 Thi1s T4est 3a"  -->  "Thi1s is2 3a T4est"
// "4of Fo1r pe6ople g3ood th5e the2"  -->  "Fo1r the2 g3ood 4of th5e pe6ople"
// ""  -->  ""

function order(words){
  let tab = words.split(" ");
  let res = [];
  const reg = /[1-9]/gm;
  tab.forEach(el => {  
    let newIdx = parseInt(el.match(reg))
    res[newIdx-1] = el;
  })
return res.join(" ");
}

//-------------------------------------------------------

// digPow(89, 1) should return 1 since 8¹ + 9² = 89 = 89 * 1
// digPow(92, 1) should return -1 since there is no k such as 9¹ + 2² equals 92 * k
// digPow(695, 2) should return 2 since 6² + 9³ + 5⁴= 1390 = 695 * 2
// digPow(46288, 3) should return 51 since 4³ + 6⁴+ 2⁵ + 8⁶ + 8⁷ = 2360688 = 46288 * 51

function digPow(n, p){
  let string = n.toString();
  let sum = 0;
  let k = 0;
  for(let i = 0; i < n.toString().length; i++){
    sum += Math.pow(parseInt(string[i]), p+i);  
  }
  k = sum / n;
return (k > 0 && Number.isInteger(k)) ? k: -1;   
}
//-------------------------------------------------------

// describe("Fixed tests", function(){
//   it("It should pass fixed tests", function(){
//     Test.assertEquals(songDecoder("AWUBBWUBC"), "A B C","WUB should be replaced by 1 space");
//     Test.assertEquals(songDecoder("AWUBWUBWUBBWUBWUBWUBC"), "A B C","multiples WUB should be replaced by only 1 space");
//     Test.assertEquals(songDecoder("WUBAWUBBWUBCWUB"), "A B C","heading or trailing spaces should be removed");
//   });
// });

let song = "WUBAWUBBWUBCWUB"

function songDecoder(song){
  return song.replace(/(WUB){1,}/gm, " ").trim();
}

//-------------------------------------------------------
// Write a function that takes an integer as input, and returns the number of bits that are equal to one in the binary representation of that number. You can guarantee that input is non-negative.

// Example: The binary representation of 1234 is 10011010010, so the function should return 5 in this case
var countBits = function(n) {
  return n.toString(2).split("").filter(el => el != 0).length;
};

//-------------------------------------------------------
// alphabetPosition("The sunset sets at twelve o' clock.")
// Should return "20 8 5 19 21 14 19 5 20 19 5 20 19 1 20 20 23 5 12 22 5 15 3 12 15 3 11" (as a string)

let text = "The sunset sets at twelve o' clock."
function alphabetPosition(text) {
  const dict = {a: 1, b: 2, c: 3, d: 4, e: 5, f: 6, g: 7, h: 8, i: 9, j: 10, k: 11, l: 12, m: 13, n: 14, o: 15, p: 16, q: 17, r: 18, s: 19, t: 20, u: 21, v: 22, w: 23, x: 24, y: 25, z: 26}
  let reg = /[a-z]/gm
  return text.toLowerCase().split("").filter(el => el.match(reg)).map(el => el = dict[el]).join(" ");
}

//-------------------------------------------------------

// createPhoneNumber([1, 2, 3, 4, 5, 6, 7, 8, 9, 0]) // => returns "(123) 456-7890"
function createPhoneNumber(numbers){
  return `(${numbers[0]}${numbers[1]}${numbers[2]}) ${numbers[3]}${numbers[4]}${numbers[5]}-${numbers[6]}${numbers[7]}${numbers[8]}${numbers[9]}`
}
//-------------------------------------------------------
let morseCode = '.... . -.--   .--- ..- -.. .'
decodeMorse = function(morseCode){
  let result = morseCode.split(" ").map(el => el = MORSE_CODE[el])
  for (let i = 0; i<result.length; i++){
    if(result[i] === undefined){
      result.splice(i, 1);
    }
  }
  return result.map(el => {
    if(el === undefined){
      return el = " "
    } else {
      return el;
  }}).join("").trim();
}

//-------------------------------------------------------

// iqTest("2 4 7 8 10") => 3 // Third number is odd, while the rest of the numbers are even

// iqTest("1 2 1 1") => 2 // Second number is even, while the rest of the numbers are odd
// let numbers = "2 4 7 8 10";

function iqTest(numbers){
  let temp = numbers.split(" ").map(el => parseInt(el)).map(el => el %2);
  let sum = temp.reduce((acc, cur) => acc + cur)
  return sum === 1 ? temp.indexOf(1) +1 : temp.indexOf(0) + 1
}

//-------------------------------------------------------

let text = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";

function duplicateCount(text){
  let len = text.split("").length;
  let counter = 0;
  let temp = text.toLowerCase().split("");
  for(let i = 0; i<len; i++){
    let d = temp.shift();
    if(temp.filter(el => el === d).length > 0){
      counter++;
      temp.forEach((element, idx) => {
        if(element === d){
          temp.splice(idx, 1, undefined);
        }
      });
      temp = temp.filter(el => el !== undefined);
    }
  }
return counter;
}

//-------------------------------------------------------

// anagrams('abba', ['aabb', 'abcd', 'bbaa', 'dada']) => ['aabb', 'bbaa']

// anagrams('racer', ['crazer', 'carer', 'racar', 'caers', 'racer']) => ['carer', 'racer']

// anagrams('laser', ['lazing', 'lazy',  'lacer']) => []

let word = 'abba';
let words = ['aabb', 'abcd', 'bbaa', 'dada'];
function anagrams(word, words) {
  word = word.split("").sort().join("");
  let temp = words.map(el => el.split("").sort().join(""))
  let result = []
  temp.forEach((element, idx) => {
    if(element === word){
      result.push(words[idx])
    } 
  });
return result;
}

//-------------------------------------------------------

let iterable = 'AAAABBBCCDAABBB';
var uniqueInOrder = function(iterable){
  let temp = [];
  if(typeof iterable === "string"){  
    iterable = iterable.split("");
    for(let i = 0; i<iterable.length; i++){
      if(iterable[i+1] !== iterable[i]){
        temp.push(iterable[i])
      }
    }
  } else {
    for(let i = 0; i<iterable.length; i++){
      if(iterable[i+1] !== iterable[i]){
        temp.push(iterable[i])
      }
    }
  }
 return temp;
}

//-------------------------------------------------------

let str = 'Pig latin is cool !';
function pigIt(str){
  let result = str.split(" ").map(el => el.split(""))
  let first;
  const reg = /[a-z]/gmi;
  for(let i = 0; i<result.length; i++){
    for(let j=0; j<result[i].length; j++){
      first = result[i][j];
      result[i].shift();
      result[i].push(first);
      if(result[i].join("").match(reg)){
        result[i].push("ay");
      }
      result[i]=result[i].join("");
      break;    
    }
  }
return result.join(" ");
}

//-------------------------------------------------------

// moveZeros([false,1,0,1,2,0,1,3,"a"]) // returns[false,1,1,2,1,3,"a",0,0]

let arr = [false,1,0,1,2,0,1,3,"a"]
var moveZeros = function (arr) {
  let temp = arr.filter(el => el === 0)
  arr = arr.filter(el => el !== 0);
return arr.concat(temp);
}


//-------------------------------------------------------

let a = 1;
let b = 1;
let c =1;

function expressionMatter(a, b, c) {
  let result = []
  result.push(a*(b+c));
  result.push(a*b*c);
  result.push(a+b*c);
  result.push((a+b)*c);
return Math.max(...result);
}
//-------------------------------------------------------
let arr = ["NORTH", "SOUTH", "SOUTH", "EAST", "WEST", "NORTH", "WEST"]
function dirReduc(arr){
  arr=arr.map(el => el.toUpperCase());
  const dict = {"NORTH": "SOUTH","WEST": "EAST","SOUTH": "NORTH","EAST": "WEST"}
  for(let i = 0; i < temp.length; i++){
    if(arr[i] === dict[arr[i+1]]){ 
      arr.splice(i, 2);
      i=-1;
    } 
  }
return arr;
}
//-------------------------------------------------------
// describe('examples', function() {
//   it('should format correctly', function() {
//     Test.assertEquals(humanReadable(0), '00:00:00', 'humanReadable(0)');
//     Test.assertEquals(humanReadable(5), '00:00:05', 'humanReadable(5)');
//     Test.assertEquals(humanReadable(60), '00:01:00', 'humanReadable(60)');
//     Test.assertEquals(humanReadable(86399), '23:59:59', 'humanReadable(86399)');
//     Test.assertEquals(humanReadable(359999), '99:59:59', 'humanReadable(359999)');
//   });
// });

function humanReadable(seconds) { 
  
  function addZero(unit){
    return unit < 10 ? unit = `0${unit}` : unit
  }
  
  let hour = Math.floor(Math.floor(seconds/60)/60);
  let minRes =  Math.floor((seconds /60) - (hour*60))
  let secRes = seconds - (hour*60*60) -(minRes*60) 

  return `${addZero(hour)}:${addZero(minRes)}:${addZero(secRes)}`
}